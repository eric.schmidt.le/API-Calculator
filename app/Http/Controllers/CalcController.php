<?php

namespace App\Http\Controllers;


class CalcController extends Controller
{

    public function getAllCalcData()
    {
        $results = app('db')->select("SELECT * FROM calc");
        return $results;
    }

    public function getAdditionData()
    {
        $results = app('db')->select("SELECT * FROM calc WHERE operation = 'Addition'");
        return $results;
    }

    public function getSubtractionData()
    {
        $results = app('db')->select("SELECT * FROM calc WHERE operation = 'Subtraction'");
        return $results;
    }

    public function getMultiplicationData()
    {
        $results = app('db')->select("SELECT * FROM calc WHERE operation = 'Multiplication'");
        return $results;
    }

    public function getDivisionData()
    {
        $results = app('db')->select("SELECT * FROM calc WHERE operation = 'Division'");
        return $results;
    }


    public function postAdditionData($n1, $n2)
    {
        $result = $n1 + $n2;
        app('db')->select("INSERT INTO `calc` (`id`, `operation`, `n1`, `n2`, `result`, `time`) 
                                VALUES (NULL, 'Addition', '" . $n1 . "', '" . $n2 . "', '" . $result . "', CURRENT_TIMESTAMP);");
        return $result;
    }

    public function postSubtractionData($n1, $n2)
    {
        $result = $n1 - $n2;
        app('db')->select("INSERT INTO `calc` (`id`, `operation`, `n1`, `n2`, `result`, `time`) 
                                VALUES (NULL, 'Subtraction', '" . $n1 . "', '" . $n2 . "', '" . $result . "', CURRENT_TIMESTAMP);");
        return $result;
    }

    public function postMultiplicationData($n1, $n2)
    {
        $result = $n1 * $n2;
        app('db')->select("INSERT INTO `calc` (`id`, `operation`, `n1`, `n2`, `result`, `time`) 
                                VALUES (NULL, 'Multiplication ', '" . $n1 . "', '" . $n2 . "', '" . $result . "', CURRENT_TIMESTAMP);");
        return $result;
    }

    public function postDivisionData($n1, $n2)
    {
        $result = floor($n1 / $n2);
        app('db')->select("INSERT INTO `calc` (`id`, `operation`, `n1`, `n2`, `result`, `time`) 
                                VALUES (NULL, 'Division', '" . $n1 . "', '" . $n2 . "', '" . $result . "', CURRENT_TIMESTAMP);");
        return $result;
    }


}