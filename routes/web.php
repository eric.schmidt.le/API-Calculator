<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/foo', function () {
    return 'Hello World';
});


$app->group(['prefix' => 'api/v1'], function ($app) {
    $app->get('/','CalcController@getAllCalcData');
    $app->get('sum', 'CalcController@getAdditionData');
    $app->get('sub', 'CalcController@getSubtractionData');
    $app->get('multi', 'CalcController@getMultiplicationData');
    $app->get('div', 'CalcController@getDivisionData');

    $app->post('sum/{n1}/{n2}', 'CalcController@postAdditionData');
    $app->post('sub/{n1}/{n2}', 'CalcController@postSubtractionData');
    $app->post('multi/{n1}/{n2}','CalcController@postMultiplicationData');
    $app->post('div/{n1}/{n2}','CalcController@postDivisionData');
});

